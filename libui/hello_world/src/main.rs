
extern crate iui;

use iui::UI;
use iui::controls::{Window,WindowType};

const M_WIDTH : i32 = 800;
const M_HEIGHT : i32 = 600;

fn main(){
    //create a UI handle, initializing the UI library and guarding against memory unsafety
    let mainui = UI::init().unwrap();

    //make a window, or a few, with title and platform-native decorations, into which your app will be drawn
    let main_window = Window::new(&mainui, "Hello World Using iui in rust", M_WIDTH, M_HEIGHT, WindowType::NoMenubar);

    mainui.main();

}

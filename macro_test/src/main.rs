
macro_rules! inc {
    {$input: expr} => ($input + 1);
}

fn main() {
    println!("{}",inc!(1))
}

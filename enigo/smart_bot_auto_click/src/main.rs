extern crate enigo;

use std::time::Duration;
use std::thread;
use enigo::*;




fn main() {
    let mut enigo = Enigo::new();
    println!("program started");
    let mut counter : u128 = 0;
    loop{
        *&mut counter += 1;
        println!("loop {}!", counter);
        enigo.mouse_move_to(-1866, 60);
        enigo.mouse_down(MouseButton::Left);
        println!("mouse moved");
        enigo.mouse_down(MouseButton::Left);
        println!("downed 1");
        enigo.mouse_up(MouseButton::Left);
        println!("upped 1");
        println!("wait 5 sec");
        thread::sleep(Duration::from_millis(5000));
        enigo.mouse_down(MouseButton::Left);
        println!("downed 2");
        enigo.mouse_up(MouseButton::Left);
        println!("upped 2");
        println!("wait 5 sec");
        thread::sleep(Duration::from_millis(5000));
    }
}

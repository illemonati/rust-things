fn order_weight(s: &str) -> String {
    let mut weights = Vec::new();
    let we = s.split(" ");
    for weight in we{
        let w = weight_to_num(weight);
        weights.push(w);
    }
    let wl = weights.len();
    let le = weights[wl -1];
    weights.insert(0, le);
    weights.pop();
    let mut ws : String = weights.iter().map(|n|format!("{} ", n.to_string())).collect();
    ws.pop().unwrap();
    ws
}

fn weight_to_num(n: &str) -> u64 {
    let weight_ns = n.split(" ");
    let weight_ns: Vec<u64> = weight_ns.map(|n|n.parse().unwrap()).collect();
    let mut result = 0;
    for num in weight_ns {
        result += num;
    }
    result
}

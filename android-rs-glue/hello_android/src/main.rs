extern crate android_glue;
extern crate hyper;

use hyper::rt::{self, Future, Stream};
use std::env;
use std::io::{self, Write};

use hyper::{Body, Response, Server};
use hyper::service::service_fn_ok;

static PHRASE: &'static [u8] = b"OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOG";

fn main() {

    let addr = ([0, 0, 0, 0], 8080).into();

    // new_service is run for each connection, creating a 'service'
    // to handle requests for that specific connection.
    let new_service = || {
        // This is the `Service` that will handle the connection.
        // `service_fn_ok` is a helper to convert a function that
        // returns a Response into a `Service`.
        service_fn_ok(|_| {
            Response::new(Body::from(PHRASE))
        })
    };

    let server = Server::bind(&addr)
        .serve(new_service)
        .map_err(|e| ());

    android_glue::write_log(&format!("Listening on http://{}", addr));

    rt::run(server);
}

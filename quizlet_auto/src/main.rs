extern crate autopilot;

use std::time;
use std::thread;

use autopilot::key::KeyCodeConvertible;

fn main() {

    loop{
        autopilot::key::type_string("i", &[], 200., 0.);
        autopilot::key::type_string("\n", &[], 200., 0.);
        // autopilot::key::tap(autopilot::key::KeyCode::Return, &[], 200)
        autopilot::mouse::click(autopilot::mouse::Button::Left, Some(100));
        thread::sleep(time::Duration::from_millis(2000));
    }
}

use std::fs;
use std::path;
use std::env::{args, current_exe};
use std::process::Command;
use std::thread;
use std::time::Duration;

fn main() {
    let which = path::Path::new("fs").exists();
    if which {fs::rename("fs", "sf");}
    else {fs::rename("sf", "fs");}
    thread::sleep(Duration::new(2, 0));
    spawn_subprocess();
}


fn spawn_subprocess(){
    match Command::new(format!("{}", current_exe().unwrap().to_string_lossy()))
            .spawn(){
                Ok(_) => {},
                Err(e) => {eprintln!("{:?}", e); return;}
            };
}

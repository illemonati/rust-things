//from the tayler series
extern crate factorial;
use factorial::Factorial;
use std::env::args;

const PRECESSION : u32 = 1000;

fn main(){
    let args : Vec<String> = args().collect();
    let second_arg = args[1].trim();
    let num_to_be_processed : f64 = second_arg.parse().expect("PUT IN A Unsigned 32bit integer.");
    println!("{:?}", calc_each(25u128, num_to_be_processed));
}

fn calc_each(n: u128, x: f64) -> f64{
    ((-1 as i32).pow(n as u32) / ((2u128 * n).checked_factorial()).unwrap() as i32) as f64 * x.powf(2f64 * n as f64)
}

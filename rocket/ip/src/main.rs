#![feature(plugin)]
#![plugin(rocket_codegen)]
#[macro_use] extern crate rocket_contrib;

extern crate rocket;
use rocket_contrib::{Json, Value};
use rocket::request::{self, Request, FromRequest, Outcome};
use rocket::Outcome::Success;




struct IP {
    ip : String,
    port : u16,
}

impl<'ip, 'r> FromRequest<'ip, 'r> for IP {
    type Error = ();

    fn from_request(request: &'ip Request<'r>) -> request::Outcome<IP, ()> {
        let ip = request.remote().unwrap();
        let ipip : String = ip.ip().to_string();
        let port : u16 = ip.port();
        let ip = IP{ip: ipip, port :port};
        return Success(ip);
    }
}



#[get("/hello/<name>/<age>")]
fn hello_na(name: String, age: u8) -> String {
    format!("Hello, {} year old named {}!", age, name)
}


#[get("/hello")]
fn hello2() -> &'static str{
    "Hello World"
}

#[get("/")]
fn ip(ip: IP) -> Json<Value> {
    let port = ip.port;
    let ip = ip.ip;
    Json(json!({
        "ip": ip,
        "port" : port,
    }))
}


fn main() {
    rocket::ignite().mount("/", routes![ip,hello_na,hello2]).launch();
}

#![crate_type = "cdylib"]

use std::ffi::CString;

#[no_mangle]
pub extern fn hello_cdylib() -> CString{
    CString::new("Hello Cdylib").unwrap()
}

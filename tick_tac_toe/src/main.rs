use std::io;
use std::process;
// use std::ops::Range;

// Utlility
pub fn debug(output : &str){
    println!("DEBUG: {}",output);
}
//End Utility

fn main() {
    main_loop();
}

fn main_loop(){
    welcome();
    let user_piece = String::from("O");
    let mut boardstate:Vec<String> = Vec::new();
    for _ in 0..9{
        boardstate.push("O".to_string());
    }
    loop{
        draw_grid(&boardstate);
        user_move(&mut boardstate, &user_piece);

    }
}

fn welcome(){
    println!("Welcome to tic_tac_toe, would thou like to play? (y/n)");
    let mut resp : String = String::new();
    io::stdin().read_line(&mut resp)
        .expect("Cannot read thou line !!!");
    resp = resp.trim().to_lowercase();
    debug(&resp);

    if resp.contains("n"){
        debug("exited");
        process::exit(1);
    }
    debug("continued");
}

fn draw_grid(boardstate : & Vec<String>){
    println!("  {} | {} | {} ", &boardstate[0], &boardstate[1], &boardstate[2]);
    println!("-------------");
    println!("  {} | {} | {} ", &boardstate[3], &boardstate[4], &boardstate[5]);
    println!("-------------");
    println!("  {} | {} | {} ", &boardstate[6], &boardstate[7], &boardstate[8]);
    print!("\n", );
    debug("board Drawn");
}

fn user_move(boardstate : &mut Vec<String>, user_piece: &String){
    loop{
        let mut user_choice:String = String::new();
        println!("Please chose a square (1-9)");
        io::stdin().read_line(&mut user_choice)
            .expect("Cannot read thou line !!!");

        let mut user_choice: usize = match user_choice.trim().to_string().parse(){
            Ok(num) => num,
            Err(_) => {println!("Your Number could not be processed !!!\n"); continue;},
        };
        debug(&user_choice.to_string());
    }
    // &boardstate[user_choice] = &user_piece;
}

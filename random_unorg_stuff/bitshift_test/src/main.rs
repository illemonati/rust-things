fn main() {

    //Bitshift left
    let a : isize = 1;
    println!("{:?}", a);
    let b = (10 << &a);
    println!("{:?}", b);
    let c = (&a * 2 * 10);
    println!("{:?}", c);

    println!("");

    //bitshift right
    let d : isize = 10;
    println!("{:?}",d );
    let e = &d >> 2;
    println!("{:?}", e);
    let f = &d / (2 * 2);
    println!("{:?}", f);

    println!("");

    //bit wise and
    let h :isize = 2000;
    println!("{:?}", h);
    println!("{:?}", odd_even_fast(h));
    println!("{:?}", odd_even_slow(h));

    println!("");

    //bit wise not
    //in most languges it is ~
    //in rust it is !
    let i : isize = 1;
    println!("{:?}", !i);

    //random stuff

    let mut ivec : Vec<i32> = Vec::new();

    for i in 1..5 {
        &ivec.push(i);
    }

    for (index, num) in (&ivec).iter().enumerate() {
        println!("{}: {}", index, num);
    }
}

fn odd_even_fast(n : isize) -> &'static str{
    if (n & 1 == 1) {"odd"} else {"even"}
}

fn odd_even_slow(n : isize) -> &'static str{
    if (n % 1 == 0) {"even"} else {"odd"}
}

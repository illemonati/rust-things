extern crate hyper;
extern crate hyper_tls;

use std::io::{self, Write};
use hyper::Client;
use hyper_tls::HttpsConnector;
use hyper::rt::{self, Future, Stream};
use std::process;
use std::env;

fn main() {
    // let url = "http://google.com";
    let url : String = String::from(get_url());
    let url : hyper::Uri = url.parse::<hyper::Uri>().unwrap();
    rt::run(fetch_url(url));
}

fn get_url() -> String{
    let url = match env::args().nth(1) {
        Some(url) => url,
        None => {
            println!("Please only give 1 <url> as argument !!!");
            process::exit(1);
        },
    };
    return url;
}


fn fetch_url(url: hyper::Uri) -> impl Future<Item=(), Error=()> {

    let https = HttpsConnector::new(4).expect("TLS initialization failed");
    let client = Client::builder()
        .build::<_, hyper::Body>(https);

    client
        .get(url)
        .and_then(|res| {
            println!("Response: {}", res.status());
            println!("Headers: {:#?}", res.headers());
            println!();
            res.into_body().for_each(|chunk| {
                io::stdout().write_all(&chunk)
                    .map_err(|e| panic!("Expects stdout is open, error={}", e))
            })
        })
        .map_err(|err| {
            eprintln!("Error {}", err);
        })
}

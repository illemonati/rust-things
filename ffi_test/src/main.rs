extern crate libloading as lib;

fn cbrt(n : f64) -> lib::Result<f64> {
    let lib = lib::Library::new("cbrt.dll")?;
    unsafe {
        let func: lib::Symbol<unsafe extern fn(n: f64) -> f64> = lib.get(b"cbrt")?;
        Ok(func(n))
    }
}

fn main(){
    println!("{}", cbrt(10f64).unwrap());
}

// extern crate simplelog;

#![windows_subsystem = "windows"]
extern crate discord_rpc_client;
extern crate rand;

use std::io;
use std::thread;
use std::time;
// use simplelog::*;
use discord_rpc_client::Client as DiscordRPC;
use discord_rpc_client::models::Event;
use rand::Rng;

fn main() {
    // TermLogger::init(LevelFilter::Debug, Config::default()).unwrap();
    let one_s = time::Duration::from_millis(1000);
    let mut drpc = DiscordRPC::new(501522059571101706)
        .expect("Failed to create client");

    drpc.start();

    let vec_of_states = vec!{"never","gonna","give", "you" , "Up"};
    let mut rng = rand::thread_rng();
    let mut counter = 0;
    loop {
        // let mut buf = String::new();

        // io::stdin().read_line(&mut buf).unwrap();
        // buf.pop();

        // if buf.is_empty() {
        //     if let Err(why) = drpc.clear_activity() {
        //         println!("Failed to clear presence: {}", why);
        //     }
        // } else {
        //     if let Err(why) = drpc.set_activity(|a| a
        //         .state(buf)
        //         .assets(|ass| ass
        //             .large_image("pickle_rick")
        //             .large_text("RICK")
        //             .small_image("morty")
        //             .small_text("MORTY")))
        //     {
        //         println!("Failed to set presence: {}", why);
        //     }
        // }
        // let state = vec_of_states.get((&mut rng).gen_range(0,(&vec_of_states).len())).unwrap().to_string();
        // let m_counter = &mut counter;
        if counter == (vec_of_states.len() - 1){
            counter = 0;
        }else{
            counter += 1;
        }

        let state = vec_of_states.get(counter).unwrap().to_string();
        if let Err(why) = drpc.set_activity(|a| a
                .state(state)
                .assets(|ass| ass
                    .large_image("pickle_rick")
                    .large_text("RICK")
                    .small_image("morty")
                    .small_text("ROLLED")))
        {
            println!("Failed to set presence: {}", why);
        }
        thread::sleep(one_s);

    };
}

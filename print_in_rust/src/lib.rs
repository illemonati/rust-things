#[macro_use]
extern crate cpython;

use cpython::{Python, PyResult};

fn printr(_py: Python, val: &str) -> PyResult<String> {
    println!("{:?}", val);
    Ok("Done".to_string())
}

py_module_initializer!(libprint_in_rust, initlibprint_in_rust, PyInit_print_int_rust, |py, m | {
    try!(m.add(py, "__doc__", "Print thing but from rust"));
    try!(m.add(py, "printr", py_fn!(py, printr(val: &str))));
    Ok(())
});

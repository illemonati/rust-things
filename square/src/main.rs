
use std::env;
use std::process;

fn main() {
    // if let Some(arg1) = env::args().nth(1) {
    //     println!("The first argument is {}", arg1);
    // }
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("Please Enter exactly 1 argument !!!");
        process::exit(2);
    }

    let mut number_to_convert: isize = match args[1].parse(){
        Ok(n) => n,
        Err(_) => {
            println!("Error, try again!");
            process::exit(2);
        },
    };

    number_to_convert = &number_to_convert * &number_to_convert;
    println!("{}",number_to_convert);
}

extern crate orca;
extern crate serde_json;

use serde_json::from_str;
use serde_json::value::Value;
use orca::App;
use orca::Sort;

fn input(query: &str) -> String {
	use std::io::Write;
	let stdin = std::io::stdin();
	print!("{}", query);
	std::io::stdout().flush().unwrap();
	let mut input = String::new();
	stdin.read_line(&mut input).unwrap();
	input.trim().to_string()
}

fn main() {
	println!("Please enter the requested information");
	let username = input("Username: ");
	let password = input("Password: ");
	let id = "W_cOU_aoTd2Kug";
	let secret = "Gbk2dHc1aEozn_hkbjdmznh5xDc";

	let mut reddit = App::new("1", "1.0", "1").unwrap();
	reddit.authorize_script(&id, &secret, &username, &password).unwrap();

	let all = reddit.get_posts("all", Sort::Hot).unwrap();

	let v: Vec<Value> = serde_json::from_value(all).unwrap();
	for item in &v {
	    println!("{}\n", item);
	}

}

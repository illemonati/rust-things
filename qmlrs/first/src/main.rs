#[macro_use]
extern crate qmlrs;


struct Multiply;
impl Multiply {
    fn calculate(&self, x: isize, y: isize) -> isize {
        x * y
    }
}

Q_OBJECT! { Multiply:
    slot fn calculate(isize, isize);
}

fn main() {
    let mut engine = qmlrs::Engine::new();

    engine.set_property("multiply", Multiply);
    engine.load_local_file("multiply_ui.qml");

    engine.exec();
}

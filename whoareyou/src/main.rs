extern crate rand;
use rand::prelude::*;

fn main() {
    let mut rng = thread_rng();
    let vec_of_phrases = vec!{"Who can I be?", "Who do you think?", "YOU", "G", "O", "D", "I"};
    let phrase = vec_of_phrases.get(rng.gen_range(0,vec_of_phrases.len())).unwrap();
    println!("{}", phrase);
}

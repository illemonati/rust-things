extern crate rand;
use rand::Rng;
use std::io;
use std::cmp::Ordering;

fn main() {
    let sec_num: isize = rand::thread_rng().gen_range(1,1001);
    println!("Guess a number between 1 - 1000 !!!");
    // println!("The secret number is {}", sec_num);

    loop{
        println!("Input thou guess !!!");
        let mut guess: String = String::new();
        io::stdin().read_line(&mut guess)
            .expect("Cannot read thou line !!!");

        // let guess: i64 = guess.trim().parse()
        //     .expect("Could not parse the number");
        // println!("Thou guessed: {} ", guess);

        let guess: isize = match guess.trim().parse(){
            Ok(num) => num,
            Err(_) => {
                println!("Your Number could not be processed !!!\n");
                continue;
            },
        };
        println!("Thou guessed: {} ", guess);

        match guess.cmp(&sec_num){
            Ordering::Less => println!("Too Tiny\n"),
            Ordering::Equal => {
                println!("Just Right\n");
                break;
            },
            Ordering::Greater => println!("Too large\n"),
        };
    };

}


// extern crate num;

// use num::Num;
#![feature(link_args)]
#[link_args = "-s EXPORTED_FUNCTIONS=['_cbrt']"]
extern {}


#[no_mangle]
pub fn cbrt(n: f64) -> f64 {
    let mut start = 0f64;
    let mut end:f64 = n;
    let e = 0.0000000000001f64;
    let mut mid:f64 ;
    loop{
        mid = (start + end)/2f64;
        if ((n - mid*mid*mid).abs()) < e {
            // println!("{}",mid);
            return mid;
        }
        if (mid*mid*mid) > n {
            end = mid;
        }else{
            start = mid;
        }
    }
}

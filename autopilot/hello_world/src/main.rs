extern crate autopilot;

fn main() {
    autopilot::key::type_string("Hello, world!", &[], 200., 0.);
    let _ = autopilot::alert::alert("Hello, world!", None, None, None);
}
